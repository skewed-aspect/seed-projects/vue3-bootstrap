//----------------------------------------------------------------------------------------------------------------------
// Vite Config
//----------------------------------------------------------------------------------------------------------------------

import 'dotenv/config';
import { defineConfig } from 'vite';

// Vite Plugins
import checker from 'vite-plugin-checker';
import vue from '@vitejs/plugin-vue';
import Components from 'unplugin-vue-components/vite';
import { BootstrapVueNextResolver } from 'bootstrap-vue-next';

// Interfaces
import { ServerConfig } from './src/common/interfaces/config';

// Utils
import configUtil from '@strata-js/util-config';

// ---------------------------------------------------------------------------------------------------------------------
// Configuration
// ---------------------------------------------------------------------------------------------------------------------

const env = (process.env.ENVIRONMENT ?? 'local').toLowerCase();
configUtil.load(`./config/${ env }.yml`);

const config = configUtil.get<ServerConfig>();

//----------------------------------------------------------------------------------------------------------------------

export default defineConfig({
    root: 'src/client',
    publicDir: 'assets',
    plugins: [
        checker({
            eslint: {
                lintCommand: 'eslint "../../src/**/*.{ts,js,vue}" --max-warnings=0',
                useFlatConfig: true,
            },
            typescript: true,
            vueTsc: true,
        }),
        vue(),
        Components({
            resolvers: [ BootstrapVueNextResolver() ],
        }),
    ],
    css: {
        preprocessorOptions: {
            scss: {
                quietDeps: true,
            },
        },
    },
    server: {
        host: config.http.host,
        port: config.http.port,
        proxy: {
            '/api': `http://localhost:${ config.http.port - 1 }`,
            '/auth': `http://localhost:${ config.http.port - 1 }`,
            '/version': `http://localhost:${ config.http.port - 1 }`,
            // TODO: Add when socket.io is implemented
            // '/socket.io': {
            //     target: `http://localhost:${ config.http.port - 1 }`,
            //     ws: true,
            // },
        },
    },
    define: {
        __APP_VERSION__: JSON.stringify(process.env.npm_package_version),
    },
    build: {
        outDir: '../../dist/client',
        emptyOutDir: true,
        cssCodeSplit: true,
    },
});

//----------------------------------------------------------------------------------------------------------------------
