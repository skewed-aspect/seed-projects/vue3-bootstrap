# {{ tmplr.project_name }}

This is just a simple Vue3 + Typescript + BootstrapVueNext project.

## Getting Started

To get started, run `npm install`, and then kick off the dev server with `npm run dev`. To build for production, 
run `npm run build`.

## Next Steps

This is just a simple seed project. You'll probably want to do the following:

* Update the `README.md` with your project information.
* Update the `LICENSE` with your project information.
* Get started making something cool.
* ???
* Profit.
