# Vue 3 + Typescript + BootstrapVueNext

A simple seed project set up for the latest Bootstrap, Vue3, and BootstrapVueNext.

## Usage

This project uses a template system to allow for customization when you pull it down. To use this project simply do 
the following (replacing `<my-project>` with the name of your project):

```bash
npx tmplr gitlab:skewed-aspect/seed-projects/vue3-bootstrap --subgroup -d <my-project>
```

Then just answer the prompts.

## Development

You can run the project in development mode by running:

```bash
npm run dev
```

This will run the project in development mode with hot reloading. There may be some unrendered template strings, but 
it should all be functional.

Testing this project with template replacement is a bit of a pain in the ass, I won't lie. The reason for it is that 
you need to generate a `tmplr` preview project and then run _that_ project. So, to make things easier, we have a 
couple of scripts.

### Generate

Currently, `tmplr` doesn't support modifying file (just template replacement), so modifying `package.json` is 
basically not possible. So, instead of maintaining two copies of the same files, we generate a templated version of 
`package.json`. We still have to keep this in sync, but there's only so much we can do about that.

### Previewing

This will generate a copy of the project, prompt for the template generation, and then npm install and run the preview.

```bash
npm run tmplr:preview
```

### Cleaning

If you need to clean up the project, just run:

```bash
npm run tmplr:clean
```

### Templating

If you want to modify the templating, read up on `tmplr` from here:

* https://github.com/loreanvictor/tmplr
