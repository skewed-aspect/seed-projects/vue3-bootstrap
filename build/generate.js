// ---------------------------------------------------------------------------------------------------------------------
// Generate Template Files
// ---------------------------------------------------------------------------------------------------------------------

import { readFileSync, writeFileSync } from 'node:fs';
import { resolve } from 'node:path';

// ---------------------------------------------------------------------------------------------------------------------
// Generate templated `package.json`
// ---------------------------------------------------------------------------------------------------------------------

const scriptsToRemove = [
    // 'postinstall',
];

// Read package.json
const packagePath = resolve(import.meta.dirname, '../package.json');
const pkgJSON = JSON.parse(readFileSync(packagePath, 'utf8'));

const template = { ...pkgJSON };

// Modify fields
template.name = '{{ tmplr.shortname }}';
template.description = '{{ tmplr.description }}';
template.version = '0.0.0';

// Remove any scripts that start with `tmplr:`
for (const key in template.scripts)
{
    if (scriptsToRemove.includes(key) || key.startsWith('tmplr:'))
    {
        delete template.scripts[key];
    }
}

// Write file back
const templatePath = resolve(import.meta.dirname, '../package.tmpl.json');
writeFileSync(templatePath, JSON.stringify(template, null, 2));

// ---------------------------------------------------------------------------------------------------------------------
