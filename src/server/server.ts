// ---------------------------------------------------------------------------------------------------------------------
// Server
// ---------------------------------------------------------------------------------------------------------------------

import { resolve } from 'node:path';
import http from 'node:http';
import { AddressInfo } from 'node:net';

import 'dotenv/config';
import express, { Request, Response } from 'express';
import configUtil from '@strata-js/util-config';
import logging from '@strata-js/util-logging';

// Interfaces
import { ServerConfig } from '../common/interfaces/config.js';

// Routes
import versionRouter from './routes/version.js';

// Utils
import program from './utils/args.js';
import { errorLogger, requestLogger, serveIndex } from './routes/utils/index.js';
import { getVersion } from './utils/version.js';

// ---------------------------------------------------------------------------------------------------------------------
// Server Configuration
// ---------------------------------------------------------------------------------------------------------------------

const env = (process.env.ENVIRONMENT ?? 'local').toLowerCase();
configUtil.load(`./config/${ env }.yml`);

const config = configUtil.get<ServerConfig>();

// ---------------------------------------------------------------------------------------------------------------------

if(config.logging)
{
    logging.setConfig(config.logging);
}

const logger = logging.getLogger('server');

// ---------------------------------------------------------------------------------------------------------------------

async function main() : Promise<void>
{
    let devMode = false;
    if(program.args.includes('--dev'))
    {
        devMode = true;
    }

    // Get version
    const version = await getVersion();

    // Build express app
    const app = express();

    // Basic request logging
    app.use(requestLogger(logger));

    app.use(express.json());

    //------------------------------------------------------------------------------------------------------------------
    // Routing
    //------------------------------------------------------------------------------------------------------------------

    // Serve static files
    app.use(express.static(resolve(import.meta.dirname, '..', 'client')));

    // Application routes
    app.use('/version', versionRouter);

    // Server index.html for any html requests, but 404 everything else
    app.get(/(.*)/, (_request, response) =>
    {
        response.format({
            html: serveIndex,
            json: (_req : Request, resp : Response) =>
            {
                resp.status(404).end();
            },
        });
    });

    // Basic error logging
    app.use(errorLogger(logger));

    //------------------------------------------------------------------------------------------------------------------
    // Server
    //------------------------------------------------------------------------------------------------------------------

    const server = http.createServer(app);

    // TODO: socket-io here

    let httpPort = config.http.port;
    if(devMode)
    {
        httpPort -= 1;
    }

    server.listen(httpPort, config.http.host, () =>
    {
        const { address, port } = server.address() as AddressInfo;
        const host = [ '::', '0.0.0.0' ].includes(address) ? 'localhost' : address;
        let actualPort = port;

        if(devMode)
        {
            logger.debug('Launching vite...');
            actualPort += 1;

            // Start Vite Dev Server
            (async() =>
            {
                const { createServer } = await import('vite');
                const viteServer = await createServer();
                await viteServer.listen();
            })();
        }

        const url = `http://${ host }:${ actualPort }`;
        logger.info(`{{ tmplr.project_name }} Server v${ version } listening at ${ url }`);
    });
}

// ---------------------------------------------------------------------------------------------------------------------

main()
    .catch((error) =>
    {
        logger.error('Unexpected error, exiting. Error was:', error.stack);
    });

// ---------------------------------------------------------------------------------------------------------------------
