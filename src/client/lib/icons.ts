// ---------------------------------------------------------------------------------------------------------------------
// FontAwesome Icons
// ---------------------------------------------------------------------------------------------------------------------

// To add icons, simply use the export shorthand and export the icons under any name you'd like, as long as the
// versions from the different styles don't conflict. As long as the icon is exported from this file, it will be added
// to the library and will be made available for use.

// ---------------------------------------------------------------------------------------------------------------------

// Font Awesome Solid
export {
    faHome,
} from '@fortawesome/free-solid-svg-icons';

// Font Awesome Regular
export {
    faCircleQuestion as farCircleQuestion,
} from '@fortawesome/free-regular-svg-icons';

// Font Awesome Brands
export {
    faGitlab,
} from '@fortawesome/free-brands-svg-icons';

// ---------------------------------------------------------------------------------------------------------------------
