/* eslint-disable */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

/* prettier-ignore */
declare module 'vue' {
  export interface GlobalComponents {
    BButton: typeof import('bootstrap-vue-next/components/BButton')['BButton']
    BCard: typeof import('bootstrap-vue-next/components/BCard')['BCard']
    BCardText: typeof import('bootstrap-vue-next/components/BCard')['BCardText']
    BCardTitle: typeof import('bootstrap-vue-next/components/BCard')['BCardTitle']
    BContainer: typeof import('bootstrap-vue-next/components/BContainer')['BContainer']
    BDropdownItem: typeof import('bootstrap-vue-next/components/BDropdown')['BDropdownItem']
    BNavbar: typeof import('bootstrap-vue-next/components/BNavbar')['BNavbar']
    BNavbarBrand: typeof import('bootstrap-vue-next/components/BNavbar')['BNavbarBrand']
    BNavbarNav: typeof import('bootstrap-vue-next/components/BNavbar')['BNavbarNav']
    BNavItem: typeof import('bootstrap-vue-next/components/BNav')['BNavItem']
    BNavItemDropdown: typeof import('bootstrap-vue-next/components/BNav')['BNavItemDropdown']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
  }
}
