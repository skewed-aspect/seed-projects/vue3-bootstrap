// ---------------------------------------------------------------------------------------------------------------------
// Main App
// ---------------------------------------------------------------------------------------------------------------------

import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { createBootstrap } from 'bootstrap-vue-next';

// Managers
import versionMan from './lib/managers/version.js';

// Components
import AppComponent from './app.vue';

// Pages
import MainPage from './pages/mainPage.vue';
import AboutPage from './pages/aboutPage.vue';

// Site Style
import './scss/theme.scss';

// Site Icons
import * as icons from './lib/icons.js';

// ---------------------------------------------------------------------------------------------------------------------
// Vue Router
// ---------------------------------------------------------------------------------------------------------------------

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'home',
            component: MainPage,
        },
        {
            path: '/about',
            name: 'about',
            component: AboutPage,
        },

        // Fallback to home
        {
            path: '/:catchAll(.*)',
            redirect: { name: 'home' },
        },
    ],
});

// ---------------------------------------------------------------------------------------------------------------------
// Font Awesome
// ---------------------------------------------------------------------------------------------------------------------

library.add(icons);

// ---------------------------------------------------------------------------------------------------------------------
// Vue App
// ---------------------------------------------------------------------------------------------------------------------

// Create the font awesome component
const app = createApp(AppComponent)
    .component('FaIcon', FontAwesomeIcon)
    .use(createBootstrap())
    .use(router);

// ---------------------------------------------------------------------------------------------------------------------

// Initialization
async function init() : Promise<void>
{
    // Initialize Managers
    await versionMan.init();

    // Mount the app
    app.mount('#main-app');

    // Log initialization
    console.log('{{ tmplr.project_name }} initialized.');
}

// ---------------------------------------------------------------------------------------------------------------------

init()
    .catch((err) =>
    {
        console.error('Failed to initialize application:', err.stack);
    });

// ---------------------------------------------------------------------------------------------------------------------
