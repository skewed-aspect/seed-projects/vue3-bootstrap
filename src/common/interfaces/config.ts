// ---------------------------------------------------------------------------------------------------------------------
// Configuration Interfaces
// ---------------------------------------------------------------------------------------------------------------------

import { LoggerConfig } from '@strata-js/util-logging';

// ---------------------------------------------------------------------------------------------------------------------

export interface HttpConfig
{
    host : string;
    port : number;
    secure : boolean;
}

export interface GoogleAuthConfig
{
    clientID : string;
    clientSecret : string;
}

export interface AuthConfig
{
    session : {
        key : string;
        secret : string;
    },
    google : GoogleAuthConfig;
    users ?: {
        admins : string[];
    }
}

export interface ServerConfig
{
    http : HttpConfig;
    auth : AuthConfig;
    logging ?: LoggerConfig;
}

// ---------------------------------------------------------------------------------------------------------------------
